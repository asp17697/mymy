import pandas as pd
import numpy as np
import time
import datetime
#from stable_baselines.common.vec_env import DummyVecEnv

# preprocessor
#from preprocessing.preprocessor import *
# config
from config.config import *
# model
from model.models import *
import os

def run_model(share: str) -> None:
    preprocessed_path = f"data/done_data_{share}.csv"
    
    if os.path.exists(preprocessed_path):
        data = pd.read_csv(preprocessed_path, index_col=0)
    else:
        data = get_yahoo_data(config.START_DATE, config.END_DATE, config.SHARE)
        data = preprocess_data()
        data = add_turbulence(data)
        #data = pd.read_csv('data/done_data.csv')
        #data = add_trends_data(data)
        data.to_csv(preprocessed_path)
    
    #data = pd.read_csv(preprocessed_path)
    #data = add_trends_data(data)
    #data.to_csv(preprocessed_path)

    print(data.head())
    print(data.size
    )
    #datatimes = pd.to_datetime(data["datadate"], format='%d/%m/%Y')
    datatimes = pd.to_datetime(data["datadate"], format='%Y/%m/%d')
    data["datadate"] = datatimes
    date_init = datetime.datetime(2015,10,1)
    date_end = datetime.datetime(2021,7,30)

    unique_trade_date = data[(data.datadate > date_init)&(data.datadate <= date_end)].datadate.unique()
    print(unique_trade_date)

    rebalance_window = 63
    validation_window = 30

    run_ensemble_strategy(df=data, 
                          unique_trade_date= unique_trade_date,
                          rebalance_window = rebalance_window,
                          validation_window=validation_window)
    
if __name__ == "__main__":
    run_model(share)
