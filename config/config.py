import pathlib

#import finrl

import pandas as pd
import datetime
import os
import urllib.parse

#from preprocessing import preprocessor

TRAINING_DATA_FILE = "data/dow_30_2009_2020.csv"

now = datetime.datetime.now()
TRAINED_MODEL_DIR = f"trained_models/{now}"
os.makedirs(TRAINED_MODEL_DIR)
TURBULENCE_DATA = "data/dow30_turbulence_index.csv"

TESTING_DATA_FILE = "test.csv"

'''
In this section it gonna define de parameters to proove the model
such as:

 - hyperparameters : learning rate, revalance window, validation window, etc.
 - shares to work
 - start and end date of data

'''

start_date = "01/01/2005" #mm/dd/yyyy
end_date = "08/08/2021"
share = "BTC-USD"
START_YEAR = "2011-00-00"

date = datetime.datetime.strptime(start_date, "%m/%d/%Y")
timestamp = datetime.datetime.timestamp(date)
START_DATE = str(timestamp)[:-2]

date = datetime.datetime.strptime(end_date, "%m/%d/%Y")
timestamp = datetime.datetime.timestamp(date)
END_DATE = str(timestamp)[:-2]

SHARE = urllib.parse.quote(share)
