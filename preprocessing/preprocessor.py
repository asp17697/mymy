from os import error
import numpy as np
import pandas as pd
import requests
from stockstats import StockDataFrame as Sdf
from config import config
from pytrends.request import TrendReq

trainig_file_path = config.TRAINING_DATA_FILE

def get_yahoo_data(start_date: str, end_date: str, share: str):
    headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"}

    url = ("https://query1.finance.yahoo.com/v7/finance/download/" + share
        + "?period1=" + start_date
        + "&period2=" + end_date
        + "&interval=1d&events=history&includeAdjustedClose=true")
    r = requests.get(url, headers=headers)
    if str(r.status_code) == "200":
        path = "data/" + share + "_" + start_date + "_to_" + end_date + ".csv"
        global trainig_file_path 
        trainig_file_path = path
        with open(path, "wb") as out_file:
            out_file.write(r.content)
            out_file.close()
            return trainig_file_path
    else:
        error_message = "Bad request to Yahoo finance :("
        return error_message
    

def load_dataset(*, file_name: str) -> pd.DataFrame:
    """
    load csv dataset from path
    :return: (df) pandas dataframe
    """
    _data = pd.read_csv(file_name)
    return _data

def data_split(df,start,end):
    """
    split the dataset into training or testing using date
    :param data: (df) pandas dataframe, start, end
    :return: (df) pandas dataframe
    """
    data = df[(df.datadate >= start) & (df.datadate < end)]
    data=data.sort_values(['datadate','tic'],ignore_index=True)
    #data  = data[final_columns]
    data.index = data.datadate.factorize()[0]
    return data

def calcualte_price(df):
    """
    calcualte adjusted close price, open-high-low price and volume
    :param data: (df) pandas dataframe
    :return: (df) pandas dataframe
    """
    data = df.copy()
    data = data[['Date', 'Open', 'High', 'Low', 'Adj Close', 'Volume']]

    data['datadate'] = data['Date']
    data['adjcp'] = data['Adj Close']
    data['open'] = data['Open']
    data['high'] = data['High']
    data['low'] = data['Low']
    data['volume'] = data['Volume']
    data['tic'] = config.SHARE

    data = data[['datadate', 'tic', 'adjcp', 'open', 'high', 'low', 'volume']]
    data = data.sort_values(['datadate'], ignore_index=True)
    return data

def add_technical_indicator(df):
    """
    calcualte technical indicators
    use stockstats package to add technical inidactors
    :param data: (df) pandas dataframe
    :return: (df) pandas dataframe
    """
    stock = Sdf.retype(df.copy())

    stock['close'] = stock['adjcp']
    unique_ticker = stock.tic.unique()

    macd = pd.DataFrame()
    rsi = pd.DataFrame()
    cci = pd.DataFrame()
    dx = pd.DataFrame()

    #temp = stock[stock.tic == unique_ticker[0]]['macd']
    for i in range(len(unique_ticker)):
        ## macd
        temp_macd = stock[stock.tic == unique_ticker[i]]['macd']
        temp_macd = pd.DataFrame(temp_macd)
        macd = macd.append(temp_macd, ignore_index=True)
        ## rsi
        temp_rsi = stock[stock.tic == unique_ticker[i]]['rsi_30']
        temp_rsi = pd.DataFrame(temp_rsi)
        rsi = rsi.append(temp_rsi, ignore_index=True)

    df['macd'] = macd
    df['rsi'] = rsi

    return df

def preprocess_data():
    """data preprocessing pipeline"""

    df = load_dataset(file_name=trainig_file_path)
    # get data after start year
    df = df[df.Date>=config.START_YEAR]
    # calcualte adjusted price
    df_preprocess = calcualte_price(df)
    # add technical indicators using stockstats
    df_final=add_technical_indicator(df_preprocess)
    # fill the missing values at the beginning
    df_final.fillna(method='bfill',inplace=True)
    return df_final

def add_turbulence(df):
    """
    add turbulence index from a precalcualted dataframe
    :param data: (df) pandas dataframe
    :return: (df) pandas dataframe
    """
    #turbulence_index = calcualte_turbulence(df)
    turbulence_index = calculate_deviation(df)
    df = df.merge(turbulence_index, on='datadate')
    df = df.sort_values(['datadate','tic']).reset_index(drop=True)
    return df

def calculate_deviation(df):
    """ 
    I don't need turbulence index wether I've only one asset,
    well... I don't guess so
    """

    df_price_pivot=df.pivot(index='datadate', columns='tic', values='adjcp')
    unique_date = df.datadate.unique()
    close_data = df['adjcp']
    difference_index = []
    for i in range(len(unique_date)):
        if i > 8:
            current_price = df_price_pivot[df_price_pivot.index == unique_date[i]].iloc[0]['BTC-USD']
            week_prices = [df_price_pivot[df_price_pivot.index == unique_date[x]] for  x in range (i-8,i)]
            mean = np.mean(week_prices)
            sigma = np.std(week_prices, ddof=1)
            difference_temp = (current_price-mean)/sigma
        else:
            difference_temp = 0.0
        difference_index.append(difference_temp)
    difference_index = pd.DataFrame(({'datadate': df_price_pivot.index, 'turbulence': difference_index}))
    return difference_index

def calcualte_turbulence(df):
    """calculate turbulence index based on dow 30"""
    # can add other market assets
    
    df_price_pivot=df.pivot(index='datadate', columns='tic', values='adjcp')
    unique_date = df.datadate.unique()
    # start after a year
    start = 252
    turbulence_index = [0]*start
    #turbulence_index = [0]
    count=0
    for i in range(start,len(unique_date)):
        current_price = df_price_pivot[df_price_pivot.index == unique_date[i]]
        hist_price = df_price_pivot[[n in unique_date[0:i] for n in df_price_pivot.index ]]
        cov_temp = hist_price.cov()
        current_temp=(current_price - np.mean(hist_price,axis=0))
        temp = current_temp.values.dot(np.linalg.inv(cov_temp)).dot(current_temp.values.T)
        if temp>0:
            count+=1
            if count>2:
                turbulence_temp = temp[0][0]
            else:
                #avoid large outlier because of the calculation just begins
                turbulence_temp=0
        else:
            turbulence_temp=0
        turbulence_index.append(turbulence_temp)
    
    turbulence_index = pd.DataFrame({'datadate':df_price_pivot.index,
                                     'turbulence':turbulence_index})
    return turbulence_index

def add_trends_data(df):
    """
    add google trends data
    :param data: (df) pandas dataframe
    :return: (df) pandas dataframe
    """
    stock = df.copy()
    pytrend = TrendReq()

    kw_list = ['Tesla', 'Elon Musk', 'space x']
    pytrend.build_payload(kw_list=kw_list, timeframe = 'all')
    trends = pytrend.interest_over_time()
    stock['close'] = stock['adjcp']
    unique_ticker = stock.tic.unique()

    keyword_1 = pd.DataFrame()
    keyword_2 = pd.DataFrame()
    keyword_3 = pd.DataFrame()

    #temp = stock[stock.tic == unique_ticker[0]]['macd']
    
    for i, row in trends.iterrows():
        temp_kw1 = np.array([(row[kw_list[0]])])
        #temp_kw1 = np.array([(temp_kw1)])
        temp_kw1 = pd.DataFrame(temp_kw1)
        temp_kw2 = np.array([(row[kw_list[1]])])
        temp_kw2 = pd.DataFrame(temp_kw2)
        temp_kw3 = np.array([(row[kw_list[2]])])
        temp_kw3 = pd.DataFrame(temp_kw3)
        #print ("temp ", temp_kw1)

        for n, m in stock.iterrows():
            print ("fecha google: " , str(i)[:7], "fecha pandas: ", str(m['datadate'][:7]))
            if str(i)[:10] == str(m['datadate']) :
                keyword_1 = keyword_1.append(temp_kw1, ignore_index=True)
                keyword_2 = keyword_2.append(temp_kw2, ignore_index=True)
                keyword_3 = keyword_3.append(temp_kw3, ignore_index=True)
                print ("keyword ", keyword_1)

    df['keyword_1'] = keyword_1
    df['keyword_2'] = keyword_2
    df['keyword_3'] = keyword_3
    
    return df











